package com.instruction.queue.medium;

public interface MessageReceiver {
    void receive(String message);
}
