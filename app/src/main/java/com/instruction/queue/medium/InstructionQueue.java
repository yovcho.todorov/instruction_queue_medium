package com.instruction.queue.medium;

import java.util.ArrayList;
import java.util.List;

public class InstructionQueue {
    private List<InstructionMessage> messages;

    public InstructionQueue() {
        messages = new ArrayList<>();
    }

    public void enqueue(InstructionMessage message) {
        messages.add(message);
    }

    public InstructionMessage[] show() {
        return messages.toArray(new InstructionMessage[0]);
    }

    public int count() {
        return messages.size();
    }

    public boolean isEmpty() {
        return messages.isEmpty();
    }
}
