package com.instruction.queue.medium;

import java.text.ParseException;

public class InstructionMessageReceiver implements MessageReceiver {

    private final InstructionQueue queue;

    public InstructionMessageReceiver() {
        this.queue = new InstructionQueue();
    }

    @Override
    public void receive(String message) {
        try {
            InstructionMessage instructionMessage = new InstructionMessage(message);
            queue.enqueue(instructionMessage);
            System.out.println("Added message to queue: " + message);
        } catch (IllegalArgumentException | ParseException e) {
            System.err.println("Error: " + e.getMessage() + " Message: " + message);
        }
    }

    public InstructionQueue getQueue() {
        return queue;
    }

    public static void main(String[] args) {
        InstructionMessageReceiver receiver = new InstructionMessageReceiver();

        receiver.receive("InstructionMessage A AB12 10 50 2023-03-20T01:00:00.000Z");
        receiver.receive("InstructionMessage B CD34 20 100 2023-03-02T10:10:00.000Z");
        receiver.receive("InstructionMessage C EF56 30 150 2020-03-21T10:20:00.000Z");
        receiver.receive("InstructionMessage D GH78 40 200 1970-03-21T10:22:00.000Z");

        InstructionQueue queue = receiver.getQueue();
        InstructionMessage[] messages = queue.show();

        System.out.println("Messages in queue:");
        for (InstructionMessage message : messages) {
            System.out.println(message.toString());
        }
    }
}
