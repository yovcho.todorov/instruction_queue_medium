package com.instruction.queue.medium;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class InstructionMessage {
    private String instructionType;
    private String productCode;
    private int quantity;
    private int uom;
    private Date timestamp;

    public InstructionMessage(String message) throws ParseException {
        String[] fields = message.split(" ");
        if (fields.length != 6 || !fields[0].equals("InstructionMessage")) {
            throw new IllegalArgumentException("Invalid message format: " + message);
        }
        this.instructionType = validateInstructionType(fields[1]);
        this.productCode = validateProductCode(fields[2]);
        this.quantity = validateQuantity(fields[3]);
        this.uom = validateUOM(fields[4]);
        this.timestamp = validateTimestamp(fields[5]);
    }

    private String validateInstructionType(String field) {
        if (!field.matches("[ABCD]")) {
            throw new IllegalArgumentException("Invalid instruction type: " + field);
        }
        return field;
    }

    private String validateProductCode(String field) {
        if (!field.matches("[A-Z]{2}\\d{2}")) {
            throw new IllegalArgumentException("Invalid product code: " + field);
        }
        return field;
    }

    private int validateQuantity(String field) {
        try {
            int value = Integer.parseInt(field);
            if (value <= 0) {
                throw new IllegalArgumentException("Invalid quantity: " + field);
            }
            return value;
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Invalid quantity: " + field);
        }
    }

    private int validateUOM(String field) {
        try {
            int value = Integer.parseInt(field);
            if (value < 0 || value >= 256) {
                throw new IllegalArgumentException("Invalid UOM: " + field);
            }
            return value;
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Invalid UOM: " + field);
        }
    }

    private Date validateTimestamp(String field) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        try {
            Date timestamp = format.parse(field);
            Date currentTime = new Date();
            Date unixEpoch = format.parse("1970-01-01T00:00:00.000Z");
            if (timestamp.before(unixEpoch) || timestamp.after(currentTime)) {
                throw new IllegalArgumentException("Invalid timestamp: " + field);
            }
            return timestamp;
        } catch (ParseException e) {
            throw new IllegalArgumentException("Invalid timestamp format: " + field, e);
        }
    }

    public String getInstructionType() {
        return instructionType;
    }

    public String getProductCode() {
        return productCode;
    }

    public int getQuantity() {
        return quantity;
    }

    public int getUOM() {
        return uom;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    @Override
    public String toString() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        return String.format("InstructionMessage %s %s %d %d %s\n",
                instructionType, productCode, quantity, uom, format.format(timestamp));
    }
}
