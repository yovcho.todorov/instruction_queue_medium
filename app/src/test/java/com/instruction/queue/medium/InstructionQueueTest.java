package com.instruction.queue.medium;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.*;

public class InstructionQueueTest {

    private InstructionQueue queue;

    @BeforeEach
    void setup() {
        queue = new InstructionQueue();
    }

    @Test
    void testEnqueue() throws ParseException{
        InstructionMessage message = new InstructionMessage("InstructionMessage A AB12 10 50 2023-03-21T10:00:00.000Z\n");
        queue.enqueue(message);
        assertEquals(1, queue.count());
    }

    @Test
    void testShow() throws ParseException{
        InstructionMessage message1 = new InstructionMessage("InstructionMessage A AB12 10 50 2023-03-21T10:00:00.000Z\n");
        InstructionMessage message2 = new InstructionMessage("InstructionMessage B CD34 20 100 2023-03-21T10:05:00.000Z\n");
        queue.enqueue(message1);
        queue.enqueue(message2);
        InstructionMessage[] messages = queue.show();
        assertEquals(2, messages.length);
        assertEquals(message1, messages[0]);
        assertEquals(message2, messages[1]);
    }

    @Test
    void testCount() throws ParseException{
        assertEquals(0, queue.count());
        queue.enqueue(new InstructionMessage("InstructionMessage A AB12 10 50 2023-03-21T10:00:00.000Z\n"));
        queue.enqueue(new InstructionMessage("InstructionMessage B CD34 20 100 2023-03-21T10:05:00.000Z\n"));
        assertEquals(2, queue.count());
    }

    @Test
    void testIsEmpty() throws ParseException{
        assertTrue(queue.isEmpty());
        queue.enqueue(new InstructionMessage("InstructionMessage A AB12 10 50 2023-03-21T10:00:00.000Z\n"));
        assertFalse(queue.isEmpty());
    }
}
