package com.instruction.queue.medium;

import org.junit.jupiter.api.Test;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

public class InstructionMessageTest {

    @Test
    public void testValidMessage() throws ParseException {
        String timestampString = "2023-03-21T10:00:00.000Z";
        InstructionMessage message = new InstructionMessage("InstructionMessage A AB12 10 50 " + timestampString + "\n");
        assertEquals("A", message.getInstructionType());
        assertEquals("AB12", message.getProductCode());
        assertEquals(10, message.getQuantity());
        assertEquals(50, message.getUOM());

        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        Date timestamp = format.parse(timestampString);
        System.out.println("Timestamp string: " + timestampString);
        System.out.println("Parsed date: " + timestamp);
    }


    @Test
    public void testInvalidInstructionType() throws ParseException {
        try {
            new InstructionMessage("InstructionMessage X AB12 10 50 2023-03-21T10:00:00.000Z\n");
            fail("IllegalArgumentException not thrown");
        } catch (IllegalArgumentException e) {
            // expected exception thrown
        }
    }

    @Test
    public void testInvalidProductCode() throws ParseException {
        try {
            new InstructionMessage("InstructionMessage A abc1 10 50 2023-03-21T10:00:00.000Z\n");
            fail("IllegalArgumentException not thrown");
        } catch (IllegalArgumentException e) {
            // expected exception thrown
        }
    }

    @Test
    public void testInvalidQuantity() throws ParseException {
        try {
            new InstructionMessage("InstructionMessage A AB12 -1 50 2023-03-21T10:00:00.000Z\n");
            fail("IllegalArgumentException not thrown");
        } catch (IllegalArgumentException e) {
            // expected exception thrown
        }
        try {
            new InstructionMessage("InstructionMessage A AB12 xyz 50 2023-03-21T10:00:00.000Z\n");
            fail("IllegalArgumentException not thrown");
        } catch (IllegalArgumentException e) {
            // expected exception thrown
        }
    }

    @Test
    public void testInvalidUOM() throws ParseException {
        try {
            new InstructionMessage("InstructionMessage A AB12 10 -1 2023-03-21T10:00:00.000Z\n");
            fail("IllegalArgumentException not thrown");
        } catch (IllegalArgumentException e) {
            // expected exception thrown
        }
        try {
            new InstructionMessage("InstructionMessage A AB12 10 256 2023-03-21T10:00:00.000Z\n");
            fail("IllegalArgumentException not thrown");
        } catch (IllegalArgumentException e) {
            // expected exception thrown
        }
        try {
            new InstructionMessage("InstructionMessage A AB12 10 xyz 2023-03-21T10:00:00.000Z\n");
            fail("IllegalArgumentException not thrown");
        } catch (IllegalArgumentException e) {
            // expected exception thrown
        }
    }

    @Test
    public void testInvalidTimestamp() throws ParseException {
        try {
            new InstructionMessage("InstructionMessage A AB12 10 50 0000-00-00T00:00:00.000Z\n");
            fail("IllegalArgumentException not thrown for invalid timestamp format");
        } catch (IllegalArgumentException e) {
            // expected exception thrown
        }
        try {
            new InstructionMessage("InstructionMessage A AB12 10 50 2024-03-21T10:00:00.000Z\n");
            fail("IllegalArgumentException not thrown for future timestamp");
        } catch (IllegalArgumentException e) {
            // expected exception thrown
        }
        try {
            // Create an InstructionMessage with an invalid timestamp format
            InstructionMessage message = new InstructionMessage("InstructionMessage A AB12 10 50 invalid-timestamp\n");
            fail("IllegalArgumentException not thrown for invalid timestamp format");
        } catch (IllegalArgumentException e) {
            // Check if the expected exception was thrown
            // If the exception was thrown, the test passes
        }
    }
}